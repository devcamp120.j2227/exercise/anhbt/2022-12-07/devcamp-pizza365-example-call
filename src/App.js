import Axios from "./components/Axios";
import FetchAPI from "./components/FetchAPI";

function App() {
  return (
    <div className="container mt-5">
      <FetchAPI></FetchAPI>
      <br></br>
      <Axios></Axios>
    </div>
  );
}

export default App;
