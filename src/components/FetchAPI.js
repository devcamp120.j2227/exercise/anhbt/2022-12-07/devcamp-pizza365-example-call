import { Component } from "react"

class FetchAPI extends Component {
    getAllOrders = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
          };
          
          fetch("http://203.171.20.210:8080/devcamp-pizza365/orders", requestOptions)
            .then(response => response.text())
            .then(result => console.log(result))
            .catch(error => console.log('error', error));
    }

    createOrder = () => {
        var myHeaders = new Headers();
        myHeaders.append("Content-Type", "application/json");

        var raw = JSON.stringify({
        "kichCo": "M",
        "duongKinh": "25",
        "suon": "4",
        "salad": "300",
        "loaiPizza": "HAWAII",
        "idVourcher": "16512",
        "idLoaiNuocUong": "PEPSI",
        "soLuongNuoc": "3",
        "hoTen": "Phạm Thanh Bình",
        "thanhTien": "200000",
        "email": "binhpt001@devcamp.edu.vn",
        "soDienThoai": "0865241654",
        "diaChi": "Hà Nội",
        "loiNhan": "Pizza đế dày"
        });

        var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow'
        };

        fetch("http://203.171.20.210:8080/devcamp-pizza365/orders", requestOptions)
        .then(response => response.text())
        .then(result => console.log(result))
        .catch(error => console.log('error', error));
    }

    getOrderById = () => {
        var requestOptions = {
            method: 'GET',
            redirect: 'follow'
            };
            
            fetch("http://203.171.20.210:8080/devcamp-pizza365/orders/oe2f2iASL1", requestOptions)
            .then(response => response.text())
            .then(result => console.log(result))
            .catch(error => console.log('error', error));
    }
    
    render() {
        return (
            <div>
                <button className="btn btn-info m-1" onClick={this.getAllOrders}>Call api (FetchAPI) get all orders!</button>
                <button className="btn btn-success m-1" onClick={this.createOrder}>Call api (FetchAPI) create order!</button>
                <button className="btn btn-warning m-1" onClick={this.getOrderById}>Call api (FetchAPI) get order by id!</button>
            </div>
        )
    }
}

export default FetchAPI;