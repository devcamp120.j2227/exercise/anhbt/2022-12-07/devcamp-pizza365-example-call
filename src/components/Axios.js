import { Component } from "react"
import axios from "axios";

class Axios extends Component {
    getAllOrders = () => {

        var config = {
        method: 'get',
        url: 'http://203.171.20.210:8080/devcamp-pizza365/orders/',
        headers: { }
        };

        axios(config)
        .then(function (response) {
        console.log(JSON.stringify(response.data));
        })
        .catch(function (error) {
        console.log(error);
        });

    }

    createOrder = () => {
    var data = JSON.stringify({
      "kichCo": "M",
      "duongKinh": "25",
      "suon": "4",
      "salad": "300",
      "loaiPizza": "HAWAII",
      "idVourcher": "16512",
      "idLoaiNuocUong": "PEPSI",
      "soLuongNuoc": "3",
      "hoTen": "Phạm Thanh Bình",
      "thanhTien": "200000",
      "email": "binhpt001@devcamp.edu.vn",
      "soDienThoai": "0865241654",
      "diaChi": "Hà Nội",
      "loiNhan": "Pizza đế dày"
    });
    
    var config = {
      method: 'post',
      url: 'http://203.171.20.210:8080/devcamp-pizza365/orders/',
      headers: { 
        'Content-Type': 'application/json'
      },
      data : data
    };
    
    axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
    })
    .catch(function (error) {
      console.log(error);
    });
    }

    getOrderById = () => {
    var config = {
      method: 'get',
      url: 'http://203.171.20.210:8080/devcamp-pizza365/orders/oe2f2iASL1',
      headers: { }
    };
    
    axios(config)
    .then(function (response) {
      console.log(JSON.stringify(response.data));
    })
    .catch(function (error) {
      console.log(error);
    });}
    
    render() {
        return (
            <div>
                <button className="btn btn-info m-1" onClick={this.getAllOrders}>Call api (Axios) get all orders!</button>
                <button className="btn btn-success m-1" onClick={this.createOrder}>Call api (Axios) create order!</button>
                <button className="btn btn-warning m-1" onClick={this.getOrderById}>Call api (Axios) get order by id!</button>
            </div>
        )
    }
}

export default Axios;